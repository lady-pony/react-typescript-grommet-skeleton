FROM node:17-alpine as builder

WORKDIR /app

COPY next-env.d.ts next.config.js package.json tsconfig.json yarn.lock ./
COPY pages ./pages
COPY public ./public
COPY src ./src

RUN yarn install
RUN yarn build

FROM node:17-alpine

WORKDIR /app
COPY --from=builder /app/package.json ./package.json
COPY --from=builder /app/.next ./.next
COPY --from=builder /app/node_modules ./node_modules

EXPOSE 3000
ENTRYPOINT yarn start
