import { render } from '@testing-library/react';
import React from 'react';
import '../src/i18n/i18n';
import Home from '../src/components/home';

describe('Home', () => {
  it('renders the home page', () => {
    const { getByText } = render(<Home />);
    const text = getByText('This is the home page');
    expect(text).toBeInTheDocument();
  });

  it('translates the title to English by default', () => {
    const { getByText } = render(<Home />);
    const text = getByText('Link to the home page');
    expect(text).toBeInTheDocument();
  });
});
